﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Button_
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        TextBox tb2;
        
        public Window1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            
            SolidColorBrush brush2 = new SolidColorBrush(Colors.Red);
            button2.Background = brush2;
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            LinearGradientBrush brush1 = new LinearGradientBrush(Colors.Blue, Colors.Red, 45);

            GradientStop stop1 = new GradientStop(Colors.Blue,0);
            GradientStop stop2 = new GradientStop(Colors.Red,0.5);
            GradientStop stop3 = new GradientStop(Colors.Pink,1);

            brush1.GradientStops.Add(stop1);
            brush1.GradientStops.Add(stop2);
            brush1.GradientStops.Add(stop3);

            button1.Background = brush1;
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            listBox1.Items.Add(textBox1.Text);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void listBox1_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Point pos = e.GetPosition(listBox1);
            
            listBox1.Items.Add(pos);
        }

        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            Point pos = e.GetPosition(this);
            Title = pos.ToString();
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            RadialGradientBrush brush;
            brush = new RadialGradientBrush(Colors.White, Colors.Red);
            brush.RadiusX = 0.2;
            brush.RadiusY = 0.2;
            brush.SpreadMethod = GradientSpreadMethod.Reflect;
            //brush.GradientStops.Add(new GradientStop(Colors.Beige, 0.5));
            listBox1.Background = brush;

            ImageBrush berriesBrush = new ImageBrush();
            berriesBrush.ImageSource =
                new BitmapImage(
                    new Uri(@"kopipi2.jpg", UriKind.Relative)
                );

            // Use the brush to paint the button's background.
            button4.Background = berriesBrush;


            /*Canvas grid1 = new Canvas();
            grid1.Width = 200;
            grid1.Height = 200;
            TextBox tb1 = new TextBox();
            tb1.Width = 40;
            grid1.
            grid1.Children.Add(tb1);
            TextBox tb2 = new TextBox();
            tb2.Width = 40;
            grid1.Children.Add(tb2);*/

            /*StackPanel panel = new StackPanel();
            TextElement.SetFontSize(panel, 30);
            TextElement.SetFontStyle(panel, FontStyles.Italic);
            panel.Orientation = Orientation.Horizontal;
            panel.HorizontalAlignment = HorizontalAlignment.Center;
            TextBox helpButton = new TextBox();
            helpButton.MinWidth = 75;
            helpButton.Margin = new Thickness(10);
            helpButton.Text = "Help";
            TextBox okButton = new TextBox();
            okButton.MinWidth = 75;
            okButton.Margin = new Thickness(10);
            okButton.Text = "OK";
            panel.Children.Add(helpButton);
            panel.Children.Add(okButton);
            button4.Content = panel;*/

        }

        private void button5_Click(object sender, RoutedEventArgs e)
        {
            Window2 w1 = new Window2();
            w1.Owner = this;
            w1.ChangeColor += new EventHandler(w1_ChangeColor);
            bool? b = w1.ShowDialog();
            MessageBox.Show(b.Value.ToString());
        }

        private void textBox1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void button6_Click(object sender, RoutedEventArgs e)
        {
            Window2 w1 = new Window2();
            w1.Owner = this;
            w1.ChangeColor += new EventHandler(w1_ChangeColor);
            w1.Show();
        }

        void w1_ChangeColor(object sender, EventArgs e)
        {
            grid1.Background = Brushes.Beige;
            //((Window2)sender).textBox1.Text = "kj";
        }

        private void listBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MessageBox.Show(listBox1.Items[listBox1.SelectedIndex].ToString());
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show("edf");
        }

    }
}
